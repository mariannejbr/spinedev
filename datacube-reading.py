import struct
import numpy as np
import array as arr


#-----------------------------------
#
#-----------------------------------
def read_dvolume(filename):

    F = open(filename,'rb')
    
    #--- Read header
    head = F.read(256)
    (sizeX,) = struct.unpack('i',head[12:16])
    (sizeY,) = struct.unpack('i',head[16:20])
    (sizeZ,) = struct.unpack('i',head[20:24])
    print('>>> Reading volume of size:', sizeX,sizeY,sizeZ)

    den = arr.array('d')
    den.fromfile(F,sizeX*sizeY*sizeZ)
    F.close()
    den = np.array(den).reshape((sizeX,sizeY,sizeZ)).astype(np.float64)

    return den
                                                

#-----------------------------------
#
#-----------------------------------
def read_fvolume(filename):
    
    F = open(filename,'rb')

    #--- Read header
    head = F.read(256)
    (sizeX,) = struct.unpack('i',head[12:16])
    (sizeY,) = struct.unpack('i',head[16:20])
    (sizeZ,) = struct.unpack('i',head[20:24])
    print('>>> Reading volume of size:', sizeX,sizeY,sizeZ)
    
    den = arr.array('f')
    den.fromfile(F,sizeX*sizeY*sizeZ)    
    F.close()
    den = np.array(den).reshape((sizeX,sizeY,sizeZ)).astype(np.float32)    
    
    return den

#-----------------------------------
#
#-----------------------------------
def read_ivolume(filename):

    F = open(filename,'rb')

    #--- Read header
    head = F.read(256)
    (sizeX,) = struct.unpack('i',head[12:16])
    (sizeY,) = struct.unpack('i',head[16:20])
    (sizeZ,) = struct.unpack('i',head[20:24])
    print('>>> Reading volume of size:', sizeX,sizeY,sizeZ)
    
    den = arr.array('i')
    den.fromfile(F,sizeX*sizeY*sizeZ)
    F.close()    
    den = np.array(den).reshape((sizeX,sizeY,sizeZ)).astype(np.int32)
    
    return den

#-----------------------------------
#
#-----------------------------------
def read_bvolume(filename):

    F = open(filename,'rb')
    
    #--- Read header
    head = F.read(256)
    (sizeX,) = struct.unpack('i',head[12:16])
    (sizeY,) = struct.unpack('i',head[16:20])
    (sizeZ,) = struct.unpack('i',head[20:24])    
    print('>>> Reading volume of size:', sizeX,sizeY,sizeZ)
    
    den = arr.array('b')
    den.fromfile(F,sizeX*sizeY*sizeZ)
    F.close()    
    den = np.array(den).reshape((sizeX,sizeY,sizeZ)).astype(np.uint8)
    
    return den
