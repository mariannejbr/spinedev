#-----------------------------------
#
#-----------------------------------
def write_fvolume(vol, filename):

    shp = vol.shape

    #--- Define header
    datatype = 8
    gridtype = 0
    sizeG    = shp[0]
    sizeX    = shp[0]
    sizeY    = shp[1]
    sizeZ    = shp[2]
    offX     = 0.0
    offY     = 0.0
    offZ     = 0.0
    box      = 1.0    
    h0 = np.array([datatype, gridtype, sizeG, sizeX,sizeY,sizeZ], dtype='int32')
    h1 = np.array([offX,offY,offZ], dtype='float32')    
    h2 = np.array([box,box,box], dtype='float32')
    h3 = np.zeros(208,dtype='uint8')

    #--- Binary write
    F = open(filename, "bw")

    #--- Write header to file
    h0.tofile(F)
    h1.tofile(F)
    h2.tofile(F)
    h3.tofile(F)

    #--- write volume data
    vol.astype(dtype='float32').tofile(F)
    
    F.close()


