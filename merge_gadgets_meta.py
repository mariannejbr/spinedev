# =============================================================================#
# coding: utf-8
# intended to be used with spinedev3.5 environment from conda (python = 3.5)
# Omar Abel Rodríguez López & Mariana Jaber Bravo:   Sept 2018
# ----------||  abelrdz.mx@gmail.com  ||  jaber.mariana@gmail.com || -----------
# This script takes the read_gadget( ) function written by M. A. Aragón-Calvo
# ---------- ||     miguel.angel.aragon.calvo@gmail.com           || -----------
# and uses it to merge several gadget files into a single one with the Particle
# IDs in order and stores only their corresponding positions x, y, z.
# =============================================================================#


import sys
import numpy as np
import array as arr
from collections import OrderedDict

# Block delimiter size in bytes.
BLOCK_DELIM_SIZE = 4

# --- # --- # --- # --- # --- # --- #
# --------- READ ONE FILE ---------

def read_gadget(file_in):
    '''
    This function reads a binary file written in Gadget format and returns
    the total number of particles in the snapshot, particle IDs in this
    file and their positions.
    :param file_in:  The file to be read
    :return: header_data, Ntotal, (PID, x,y,z)
    '''
    # --- Open Gadget file
    file = open(file_in, 'rb')
    # --- Read header
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty before-delimiter
    # NumPartOfEachType_InThisFile.
    npart = np.fromfile(file, dtype='i', count=6)
    massarr = np.fromfile(file, dtype='d', count=6)
    time = (np.fromfile(file, dtype='d', count=1))[0]
    redshift = (np.fromfile(file, dtype='d', count=1))[0]
    flag_sfr = (np.fromfile(file, dtype='i', count=1))[0]
    flag_feedback = (np.fromfile(file, dtype='i', count=1))[0]
    nparttotal = np.fromfile(file, dtype='i', count=6)
    flag_cooling = (np.fromfile(file, dtype='i', count=1))[0]
    NumFiles = (np.fromfile(file, dtype='i', count=1))[0]
    BoxSize = (np.fromfile(file, dtype='d', count=1))[0]
    Omega0 = (np.fromfile(file, dtype='d', count=1))[0]
    OmegaLambda = (np.fromfile(file, dtype='d', count=1))[0]
    HubbleParam = (np.fromfile(file, dtype='d', count=1))[0]
    unused_bytes = file.read(
        256 - 6 * 4 - 6 * 8 - 8 - 8 - 2 * 4 - 6 * 4 - 4 - 4 - 4 * 8)
    # reading Gadget documentation these are the empty bytes left to
    # store a header of total length of 256 bytes
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty after-delimiter
    # End of the header block

    # Store the header as a tuple
    # convert the tuple to a dictionary
    # header_data = (
    #     block_delim, npart, massarr, time, redshift, flag_sfr, flag_feedback,
    #     nparttotal, flag_cooling, NumFiles, BoxSize, Omega0, OmegaLambda,
    #     HubbleParam, unused_bytes, block_delim
    # )

    header_data_items = [
        # ('begin_block_delim', block_delim),
        ('npart', npart),
        ('massarr', massarr),
        ('time', time),
        ('redshift', redshift),
        ('flag_sfr', flag_sfr),
        ('flag_feedback', flag_feedback),
        ('nparttotal', nparttotal),
        ('flag_cooling', flag_cooling),
        ('NumFiles', NumFiles),
        ('BoxSize', BoxSize),
        ('Omega0', Omega0),
        ('OmegaLambda', OmegaLambda),
        ('HubbleParam', HubbleParam),
        ('unused_bytes', unused_bytes),
        # ('end_block_delim', block_delim),
    ]
    header_data = OrderedDict(header_data_items)

    # --- Particles to read
    # n_all = npart[0] + npart[1] + npart[2] + npart[3] + npart[4] # original line
    # n_all = np.sum(npart)
    n_all = npart[0] + npart[1] + npart[2] + \
        npart[3] + npart[4] + npart[5]  # mjb
    # The num of part of each type in this present file
    print('>>> The num of parts of each type in THIS file are: ' + str(npart))
    print('>>> Total_no_part in the sim is ' + str(nparttotal[:]))
    ## print('n_all = ', n_all)
    nall = n_all.astype(int)

    # --------------------
    # Read positions block
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty before-delimiter
    flat_pos = np.fromfile(file, dtype='f', count=n_all * 3)
    # we need to reshape the positions to extract x,y,z separately
    pos = np.reshape(flat_pos, (3, nall))
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty after-delimiter
    # Finish reading the positions.
    # -----------------------------

    # -------------------
    # Read velocities block
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty before-delimiter
    vel = np.fromfile(file, dtype='f', count=n_all * 3)
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty after-delimiter
    # Finish reading the velocities.
    # --------------------

    # -------------------
    # Read PID block
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty before-delimiter
    part_id = np.fromfile(file, dtype='uint', count=npart[1])
    block_delim = file.read(BLOCK_DELIM_SIZE)  # 4-bytes empty after-delimiter
    # Finish reading the PIDs.
    # --------------------

    file.close()
    # --- Only dark matter particles
    x = pos[0, npart[0]:npart[0] + npart[1]]
    y = pos[1, npart[0]:npart[0] + npart[1]]
    z = pos[2, npart[0]:npart[0] + npart[1]]
    # --- dark matter particles ID
    #    pid = ID[npart[0]:npart[0]+npart[1]]
    # pid = part_id
    ntotal = nparttotal[1]  # total number of DM particles only
    # TODO: Think of a better way of extract "header_data" & maybe we won't need "ntotal" separately
    #print(">> Just for fun: look at the header_data: ", header_data)
#    print(header_data)
    return header_data, ntotal, (part_id, x, y, z)


# --- # --- # --- # --- # --- # --- #
# --------- READ SEVERAL FILES ---------
def read_gadgets(file_base_name, max_filenum):
    """
    Uses the read_gadget function to read several binary files at once.
    Returns the following information: the number of file where each part
    of the data was extracted from and the information given by read_gadget,
    Ntotal, PIDs, x, y, z
    :param file_root: Root of the name of the gadget files, for instance,
    lcdmz00015.
    :param max_filenum: Number of files among which the snapshot is distributed.
    Usually, equals to the number of processors used while running the simulation
    :return: File_idx, Header_data, Ntot, (PID, x, y, z)
    """
    # ----- Read one of the multiple files to learn the tot_num of parts
    # ----- In this case we use the COLA outputs format for the name
    gadgets_data = []
    for file_idx in range(0, max_filenum + 1):
        gadgetfile = file_base_name + '.' + str(file_idx)
        # file_data now contains: header, ntotal, pid, xyz
        file_data = read_gadget(gadgetfile)
        # (file_idx, header, ntotal, (pid, xyz))
        gadgets_data.append(file_data)

    # print(gadgets_data)
    return gadgets_data


# --- # --- # --- # --- # --- # --- #
# --- CREATE THE REFERENCE TABLES -----
def create_my_tables(gadgets_data):
    """
    Creates a reference table for each binary file that is read. This table
    contains the information of the particle IDs stored in all the files
    sorted in descending order and the index of the corresponding array element
    :param: gadgets_data:
    :return: reference_tables:
    """
    refs_tables = []
    for data in gadgets_data:
        header_data, ntot, (pidarray, x, y, z) = data
        n_file = pidarray.size

        ref_table = np.zeros((n_file, 2), dtype=np.int64)
        ref_table[:, 0] = pidarray[:]
        ref_table[:, 1] = np.arange(0, pidarray.size, dtype=np.int64)
        # --> second column => array index for each PID element
        ord_table = np.sort(ref_table.view('i8,i8'), order=['f0'], axis=0)
        refs_tables.append(ord_table.view(np.int64))
    # print(refs_tables)
    return refs_tables


# --- # --- # --- # --- # --- # --- # ---
# -- MERGE THE FILES USING THE TABLES ---
def merge_my_gadgets(gadgets_data, refs_table):
    """
    Merge all the information extracted from the binary files and stored in
    gadgets_data according to the order indicated in refs_table so that the
    particles are ordered by ID

    The merge process will also update the header data, specifically the
    fields ``npart`` and ``Numfiles``.

    :param gadgets_data: file_idx, (Header_data, Ntot, Pid, x, y, z)
    :param refs_table: PID in order, id of array
    :return: The updated header, and an array with the positions of the
        particles, x, y, z
    """
    # Just to extract the IDpart_tot we do the following:
    first_file_data = gadgets_data[0]
    header_data, IDpart_tot, _part_data = first_file_data

    # merged_data = np.zeros((4, IDpart_tot), dtype=np.float64) #<<- if we want to store ID as well
    merged_data = np.zeros((3, IDpart_tot), dtype=np.float64)  # <<-- only xyz

    for file_idx, ref_table in enumerate(refs_table):
        header_data_idx, IDpart_tot, (pid, x, y, z) = gadgets_data[file_idx]
        pid_array = ref_table[:, 0]
        pos_array = ref_table[:, 1]
        # merged_data[3, pid_array] = pid_array
        merged_data[0, pid_array] = x[pos_array]
        merged_data[1, pid_array] = y[pos_array]
        merged_data[2, pid_array] = z[pos_array]
    # assert np.alltrue(
    #    merged_data[3, :].astype(np.int64) == np.arange(0, IDpart_tot))
    # print(merged_data)

    print("The merged data has shape: ", merged_data.shape)
    #print("In the header we have: ", header_data)

    merged_header = OrderedDict(header_data)

    npart_copy = header_data['npart'].copy()
    npart_copy[1] = IDpart_tot
    merged_header['npart'] = npart_copy
    merged_header['NumFiles'] = np.array(1, dtype=np.int32)
    # print(merged_header)

    return merged_header, merged_data


def read_merged_gadgets(headers, merged_data):
    """
    """
    # headers[0], header[14] son bytes en blanco

    ntot_particles = headers['npart']
    num_files = headers['NumFiles']
    print('Number of particles in some of the headers stored is: ', headers[1])
    print('Number of total particles according to 1st header: ', headers[7])
    print('Number of files info stored in headers', headers[9])
    print('The merged data has the shape: ',  merged_data.shape)


def write_gadget_file(my_file, merged_header, merged_data):
    """
    """
    #
    with open(my_file, 'wb') as mf:
        # Let's write the header... :)
        mf.write(bytes(BLOCK_DELIM_SIZE))
        for key, value in merged_header.items():
            if key == 'unused_bytes':
                mf.write(value)
            else:
                value.tofile(mf)
        mf.write(bytes(BLOCK_DELIM_SIZE))

        # Let's write the positions
        mf.write(bytes(BLOCK_DELIM_SIZE))
        merged_data.tofile(mf)
        mf.write(bytes(BLOCK_DELIM_SIZE))


if __name__ == '__main__':
    prog_name = sys.argv[0]
    prog_args = sys.argv[1:]

    file_details = str(prog_args[0]), int(prog_args[1])
    my_file = str(prog_args[2])
    # merge_gadgets(*file_details)
    # create_table(*file_details)
    #   gadget_file_names = str(prog_args[0]), str(prog_args[1]), str(
    #       prog_args[2]), str(prog_args[3])
    # gadget_file_name = str(prog_args[0])
    # read_gadget(gadget_file_name)
    np.set_printoptions(edgeitems=30)
    gadgets_data = read_gadgets(*file_details)
    refs_tables = create_my_tables(gadgets_data)
    merged_header, merged_data = merge_my_gadgets(gadgets_data, refs_tables)

    write_gadget_file(my_file, merged_header, merged_data)

    print('SUCCESS...')
    # read_merged_gadgets(headers, merged_data)
    # ref_table = create_table2(gadgets_data)
    # merge_gadgets(gadgets_data, ref_table)
