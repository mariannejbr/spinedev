##code to combine the gadget multiple files together and do sorted according to ID and write them back in an efficent!
import numpy as np
import pandas as pd
import sys
import operator

def read_gadget(file_in):
    #--- Open Gadget file print into an array of xyz and id as one array!
    file = open(file_in,'rb')
    #--- Read header
    dummy = file.read(4)               
    npart         =  np.fromfile(file, dtype='i', count=6)
    massarr       =  np.fromfile(file, dtype='d', count=6)
    time          = (np.fromfile(file, dtype='d', count=1))[0]
    redshift      = (np.fromfile(file, dtype='d', count=1))[0]
    flag_sfr      = (np.fromfile(file, dtype='i', count=1))[0]
    flag_feedback = (np.fromfile(file, dtype='i', count=1))[0]
    nparttotal    =  np.fromfile(file, dtype='i', count=6)
    flag_cooling  = (np.fromfile(file, dtype='i', count=1))[0]
    NumFiles      = (np.fromfile(file, dtype='i', count=1))[0]
    BoxSize       = (np.fromfile(file, dtype='d', count=1))[0]
    Omega0        = (np.fromfile(file, dtype='d', count=1))[0]
    OmegaLambda   = (np.fromfile(file, dtype='d', count=1))[0]
    HubbleParam   = (np.fromfile(file, dtype='d', count=1))[0]
    header        = file.read(256-6*4 - 6*8 - 8 - 8 - 2*4-6*4 -4 -4 -4*8)
    dummy = file.read(4)

    #--- Particles to read
    n_all = npart[0]+npart[1]+npart[2]+npart[3]+npart[4]
    print('>>> '+ str(npart))
    print(' total_no_part is ' +str(nparttotal[1]))
    print n_all
    #--- Read positions
    dummy = file.read(4)
    pos = np.fromfile(file, dtype='f', count=n_all*3)
    
    # ---Read velocities
    dummy = file.read(8)
    vel = np.fromfile(file, dtype='f', count= n_all*3 )
   
 #--- Read Pid
    dummy = file.read(8)
    ID =np.fromfile(file, dtype='uint', count=npart[1]) 
  
    file.close()

    #--- Only dark matter particles
    x = pos[npart[0]:npart[0]+npart[1],0]
    y = pos[npart[0]:npart[0]+npart[1],1]
    z = pos[npart[0]:npart[0]+npart[1],2]

    #--- dark matter particles ID
#    pid = ID[npart[0]:npart[0]+npart[1]]
    pid =ID
    return x,y,z, pid