def catalogue_v0(densfile, voidfile):
    ''' This function calculates the size, mean density, variance of the density
    for each void in a given simulation. 
    It returns an array containing the ID and those properties.
    '''

    densities = read_dvolume(densfile)
    voidregs = read_ivolume_alt(voidfile)

    print('*** Finishing reading the files *** ')

    elem_voids, count_voids = unique(voidregs, return_counts=True)
    Reffbiggest = (3 * count_voids.max() / (4 * np.pi)) ** (1 / 3)


    voidproperties = []
    #for i, void_size in zip(elem_voids, count_voids):    
    for i, void_size in list(zip(elem_voids, count_voids))[0:4]:
        

        maskvoid = isin(voidregs, i)
        masked_dens = densities[maskvoid]
        meandens_forvoid = masked_dens.mean()
        variance_forvoid = masked_dens.var()
        radius_eff = (3 * count_voids[i-1] / (4 * np.pi)) ** (1 / 3)
        
        #info_to_be_stored = (i, count_voids[i-1], meandens_forvoid, variance_forvoid)
        
        info_to_be_stored = (i, radius_eff, meandens_forvoid, variance_forvoid)
        voidproperties.append(info_to_be_stored)

    print('>>> Finished storing the info for all voids! <<<')

    fname = './../data/output/S4/catalogue_v0.txt'

    Header = '''
        | Mariana Jaber 2018 | SPINEDEV project  | Using spinedev3.5 env |
        
        | Columns: | Void ID | Reff(Mpc3) | Mean(density) | Var(density) |
        '''

    np.savetxt(fname, voidproperties, header=Header)
    print('>> Saving the info as a txt file ')
    return voidproperties