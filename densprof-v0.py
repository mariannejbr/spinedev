def  dens_profiles(densfile, voidfile, distfile, th=None):
    ''' This function calculates the density profile for each void
        above certain threshold in radius, th, and stores each profile
        separately as a txt file. '''
    
    densities = read_dvolume(densfile)
    voidregs = read_ivolume_alt(voidfile)
    distances = read_bvolume(distfile)
    
    print('*** Finishing reading the files *** ')
    
    elem_voids, count_voids = unique(voidregs, return_counts=True)    

    Reffbiggest = (3 * count_voids.max()/(4*np.pi))**(1/3)
    
    print ('*** Found a total of {} voids'.format(int(elem_voids.max())))
    print ('*** The biggest has a volume of {} Mpc**3'.format(int(count_voids.max())))
    print ('*** Which corresponds to a size given by Reff={:.4g} Mpc'.format(Reffbiggest))

    for i, void_size in zip(elem_voids, count_voids):
        
        maskvoid = isin(voidregs, i)
            
        masked_dist = distances[maskvoid]

        masked_dens = densities[maskvoid] 
        
        
        dist_dens_forvoid = stack((masked_dist,masked_dens),axis=-1)
        
        rings = unique(masked_dist)
        
        th = 0 if th is None else th
        
        if rings.size >= th:
            
            Reff = (3 * void_size /(4*np.pi))**(1./3.)
            
            dplist = []
            counter = 0
            for j in rings:
                counter = counter +1
                #rbin =  Reff/(j+1)
                
               
                meandens_ring = dist_dens_forvoid[dist_dens_forvoid[:,0] == j][:,1].mean()
                
                densprof = (j, meandens_ring)
                #densprof = (j, rbin, meandens_ring)
                
                dplist.append(densprof)
        

            fname = '512all-void-{}.txt'.format(int(i))
        
            np.savetxt(fname,dplist)
            
            print('*** Saving the {}-th density profile **** '.format(int(counter)))
        